package spec

import (
	"github.com/go-openapi/spec"
	"gitlab.com/t4spartners/csm-go-lib/models"
)

func (s *Spec) InitDefinitions() {
	s.Swagger.Definitions = make(map[string]spec.Schema)
	s.Swagger.Paths.Paths = make(map[string]spec.PathItem)
	s.defaultDefinitions()

	for _, o := range s.c.Objects {
		if sch, err := s.s.O(o).Schema(); err == nil {
			s.Swagger.Definitions[o] = definition(sch)
			s.defaultPaths(o)
		}
	}
}

func definition(schema *models.SchemaResponse) spec.Schema {
	properties := make(map[string]spec.Schema, len(schema.FieldDefinitions))

	for _, f := range schema.FieldDefinitions {
		properties[f.Name] = property(f)
	}
	return spec.Schema{SchemaProps: spec.SchemaProps{Properties: properties}}
}

func (s *Spec) defaultDefinitions() {
	badProp := make(map[string]spec.Schema)
	badProp["error"] = *spec.StringProperty()

	badrequest := spec.Schema{
		SchemaProps: spec.SchemaProps{
			Properties: badProp,
		},
	}
	s.Swagger.Definitions["badrequest"] = badrequest

	existsProp := make(map[string]spec.Schema)
	existsProp["exists"] = *spec.BooleanProperty()

	exists := spec.Schema{
		SchemaProps: spec.SchemaProps{
			Properties: existsProp,
		},
	}
	s.Swagger.Definitions["exists"] = exists

	countProp := make(map[string]spec.Schema)
	countProp["count"] = *spec.Int64Property()

	count := spec.Schema{
		SchemaProps: spec.SchemaProps{
			Properties: countProp,
		},
	}
	s.Swagger.Definitions["count"] = count
}

func property(f *models.FieldDefinition) spec.Schema {
	s := spec.StringProperty()

	if f.ReadOnly {
		s.AsReadOnly()
	}

	return *s
}
