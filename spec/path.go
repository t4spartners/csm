package spec

import (
	"fmt"
	"log"

	"github.com/go-openapi/spec"
	"gitlab.com/t4spartners/csm/generator"
)

func (b *Spec) defaultPaths(object string) {
	root := spec.PathItem{}
	root.Get = b.getRootOp(object)
	// root.Post = b.postRootOp(object)
	root.Put = b.putRootOP(object)
	b.Swagger.Paths.Paths["/o/"+object] = root

	rootid := spec.PathItem{}
	rootid.Get = b.getRootIdOp(object)
	rootid.Put = b.putRootIdOp(object)
	rootid.Delete = b.deleteRootIdOp(object)

	id := spec.PathParam("id")
	id.WithDescription("The id of the " + object)
	id.Required = true
	id.Type = "string"
	rootid.Parameters = append(rootid.Parameters, *id)

	b.Swagger.Paths.Paths["/o/"+object+"/{id}"] = rootid

	exists := spec.PathItem{}
	exists.Get = b.getExistsOp(object)
	exists.Parameters = append(exists.Parameters, *id)
	b.Swagger.Paths.Paths["/o/"+object+"/{id}/exists"] = exists

	count := spec.PathItem{}
	count.Get = b.getCountOp(object)
	b.Swagger.Paths.Paths["/o/"+object+"/count"] = count

	one := spec.PathItem{}
	one.Get = b.getFindoneOp(object)
	b.Swagger.Paths.Paths["/o/"+object+"/findone"] = one
}

func (b *Spec) getRootOp(object string) *spec.Operation {
	op := spec.NewOperation("find" + generator.Pascalize(object))
	op.WithDescription("Find " + object)
	op.WithSummary("Find all instances of " + object + " matched by filter")
	op.WithTags(object)

	// Parameters
	op.AddParam(lbLimit())
	op.AddParam(lbWhere())
	op.AddParam(lbOrder())
	op.AddParam(lbField())
	op.AddParam(pagenum())
	op.AddParam(pagesize())

	// Responses
	succ := spec.NewResponse()
	succ.WithDescription("Request was successful")
	succ.WithSchema(spec.ArrayProperty(objectDefintion(object)))

	op.RespondsWith(200, succ)
	op.RespondsWith(400, badRequest())

	return op
}

func (b *Spec) postRootOp(object string) *spec.Operation {
	op := spec.NewOperation("create" + generator.Pascalize(object))
	op.WithDescription("create")
	op.WithSummary("Create instance of" + object)
	op.WithTags(object)

	// Parameters
	body := spec.BodyParam("data", objectDefintion(object))
	body.Type = ""
	body.Required = true
	op.AddParam(body)

	// Responses
	succ := spec.NewResponse()
	succ.WithDescription("Request was successful")
	succ.WithSchema(objectDefintion(object))

	op.RespondsWith(200, succ)
	op.RespondsWith(400, badRequest())

	return op
}

func (b *Spec) putRootOP(object string) *spec.Operation {
	op := spec.NewOperation("upsert" + generator.Pascalize(object))
	op.WithDescription("upsert")
	op.WithSummary("Create or update an instance of " + object)
	op.WithTags(object)

	// Paramters
	body := spec.BodyParam("data", objectDefintion(object))
	body.Type = ""
	body.Required = true
	op.AddParam(body)

	// Responses
	succ := spec.NewResponse()
	succ.WithDescription("Request was successful")
	succ.WithSchema(objectDefintion(object))

	op.RespondsWith(200, succ)
	op.RespondsWith(400, badRequest())

	return op
}

func (b *Spec) getRootIdOp(object string) *spec.Operation {
	op := spec.NewOperation("findById" + generator.Pascalize(object))
	op.WithDescription("Find " + object + " by id")
	op.WithSummary("Find instances of " + object + " by id")
	op.WithTags(object)

	// Parameters
	op.AddParam(lbField())

	// Responses
	succ := spec.NewResponse()
	succ.WithDescription("Request was successful")
	succ.WithSchema(objectDefintion(object))

	op.RespondsWith(200, succ)
	op.RespondsWith(400, badRequest())

	return op
}

func (b *Spec) putRootIdOp(object string) *spec.Operation {
	op := spec.NewOperation("updateByID" + generator.Pascalize(object))
	op.WithDescription("update by id")
	op.WithSummary("update instance of " + object + " by id")
	op.WithTags(object)

	// Paramters
	body := spec.BodyParam("data", objectDefintion(object))
	body.Type = ""
	body.Required = true
	op.AddParam(body)

	// Responses
	succ := spec.NewResponse()
	succ.WithDescription("Request was successful")
	succ.WithSchema(objectDefintion(object))

	op.RespondsWith(200, succ)
	op.RespondsWith(400, badRequest())

	return op
}

func (b *Spec) deleteRootIdOp(object string) *spec.Operation {
	op := spec.NewOperation("delete" + generator.Pascalize(object))
	op.WithDescription("delete by id")
	op.WithSummary("delete instance of " + object + " by id")
	op.WithTags(object)

	// Responses
	succ := spec.NewResponse()
	succ.WithDescription("Request was successful")

	op.RespondsWith(200, succ)
	op.RespondsWith(400, badRequest())

	return op
}

func (b *Spec) getExistsOp(object string) *spec.Operation {
	op := spec.NewOperation("exists" + generator.Pascalize(object))
	op.WithDescription("check if instance exists")
	op.WithSummary(fmt.Sprintf("Check whether an instance of %s exists", object))
	op.WithTags(object)

	// Responses
	succ := spec.NewResponse()
	succ.WithDescription("Request was successful")
	succ.WithSchema(existsDef())

	op.RespondsWith(200, succ)
	op.RespondsWith(400, badRequest())

	return op
}

func (b *Spec) getCountOp(object string) *spec.Operation {
	op := spec.NewOperation("count" + generator.Pascalize(object))
	op.WithDescription("Count instances of " + object)
	op.WithSummary("count isntances of " + object)
	op.WithTags(object)

	// Parameters
	op.AddParam(lbWhere())

	// Responses
	succ := spec.NewResponse()
	succ.WithDescription("Request was successful")
	succ.WithSchema(countDef())

	op.RespondsWith(200, succ)
	op.RespondsWith(400, badRequest())

	return op
}

func (b *Spec) getFindoneOp(object string) *spec.Operation {
	op := spec.NewOperation("findOne" + generator.Pascalize(object))
	op.Description = fmt.Sprintf("find first instance of %s matched by filter", object)
	op.Summary = fmt.Sprintf("find first instance of %s matched by filter", object)
	op.WithTags(object)

	// Parameters
	op.AddParam(lbWhere())
	op.AddParam(lbField())

	// Responses
	succ := spec.NewResponse()
	succ.WithDescription("Request was successful")
	succ.WithSchema(objectDefintion(object))

	op.RespondsWith(200, succ)
	op.RespondsWith(400, badRequest())

	return op
}

func objectDefintion(object string) *spec.Schema {
	ref, err := spec.NewRef("#/definitions/" + object)
	if err != nil {
		log.Fatal(err)
	}
	return &spec.Schema{
		SchemaProps: spec.SchemaProps{
			Ref: ref,
		},
	}
}

func badrequestDef() *spec.Schema {
	ref, err := spec.NewRef("#/definitions/badrequest")
	if err != nil {
		log.Fatal(err)
	}
	return &spec.Schema{
		SchemaProps: spec.SchemaProps{
			Ref: ref,
		},
	}
}

func countDef() *spec.Schema {
	ref, err := spec.NewRef("#/definitions/count")
	if err != nil {
		log.Fatal(err)
	}
	return &spec.Schema{
		SchemaProps: spec.SchemaProps{
			Ref: ref,
		},
	}
}

func existsDef() *spec.Schema {
	ref, err := spec.NewRef("#/definitions/exists")
	if err != nil {
		log.Fatal(err)
	}
	return &spec.Schema{
		SchemaProps: spec.SchemaProps{
			Ref: ref,
		},
	}
}

func badRequest() *spec.Response {
	resp := spec.NewResponse()
	resp.WithDescription("Error")
	resp.WithSchema(badrequestDef())
	return resp
}

// Query Parameters

func lbLimit() *spec.Parameter {
	limit := spec.QueryParam("limit")
	limit.WithDescription("Filter defining limit. Limit is currently an alias for pagesize")
	limit.Type = "integer"
	return limit
}

func lbWhere() *spec.Parameter {
	where := spec.QueryParam("where")
	where.WithDescription("Where clause in json format. where={key:value} for where the field key equal to value. Also of the form where={key:{operator:value}} where operator in [eq, lt, gt, contains, startswith]")
	where.Type = "string"
	return where
}

func lbField() *spec.Parameter {
	field := spec.QueryParam("fields")
	field.WithDescription("Fields to be included in response. If fields in nil all fields will be included.")
	field.Type = "array"
	field.Items = spec.NewItems().Typed("string", "")
	return field
}

func lbOrder() *spec.Parameter {
	order := spec.QueryParam("order")
	order.WithDescription("Filter defining order. List of fields to be sorted by.")
	order.Type = "array"
	order.Items = spec.NewItems().Typed("string", "")
	return order
}

func pagenum() *spec.Parameter {
	pageNum := spec.QueryParam("pagenum")
	pageNum.WithDescription("Page number")
	pageNum.Type = "integer"
	return pageNum
}

func pagesize() *spec.Parameter {
	pageSize := spec.QueryParam("pagesize")
	pageSize.WithDescription("Page size")
	pageSize.Type = "integer"
	return pageSize
}
