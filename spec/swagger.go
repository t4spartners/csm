package spec

import (
	"github.com/go-openapi/spec"

	csm "gitlab.com/t4spartners/csm-go-lib"
)

type Config struct {
	Server  string
	Objects []string
	Title   string
	URL     string
}

type Spec struct {
	spec.Swagger

	c *Config
	s *csm.Session
}

func New(c *Config) *Spec {
	return &Spec{
		Swagger: spec.Swagger{
			SwaggerProps: spec.SwaggerProps{
				Paths:    &spec.Paths{},
				Info:     defaultHeaders(c),
				Consumes: []string{"application/json"},
				Produces: []string{"application/json"},
				Schemes:  []string{"http"},
				Swagger:  "2.0",
				SecurityDefinitions: map[string]*spec.SecurityScheme{
					"sessionToken": cherwellOAuth(c),
				},
				Security: cherwellSecurity(),
			},
		},
		c: c,
	}
}

func (s *Spec) WithSession(sess *csm.Session) *Spec {
	s.s = sess
	return s
}

func defaultHeaders(c *Config) *spec.Info {
	var t string
	if c.Title != "" {
		t = c.Title
	} else {
		t = "worp"
	}

	return &spec.Info{
		InfoProps: spec.InfoProps{
			Version: "0.1.0",
			Title:   t,
			License: &spec.License{
				Name: "MIT",
				URL:  "https://mit-license.org",
			},
		},
	}
}

func cherwellOAuth(c *Config) *spec.SecurityScheme {
	var url string
	if c.URL != "" {
		url = c.URL
	} else {
		url = c.Server
	}

	sec := spec.OAuth2Password("http://" + url + "/CherwellAPI/token")
	sec.Description = "OAuth2 password flow"
	sec.Scopes = map[string]string{"user": "User authentication"}
	return sec
}

func cherwellSecurity() []map[string][]string {
	return []map[string][]string{
		map[string][]string{
			"sessionToken": []string{},
		},
	}
}
