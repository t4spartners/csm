// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"

	"gitlab.com/t4spartners/csm-go-lib/auth"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// keyCmd represents the key command
var keyCmd = &cobra.Command{
	Use:   "key",
	Short: "generate a cherwell access token",
	Run: func(cmd *cobra.Command, args []string) {
		var (
			url    = viper.GetString("cherwell.url")
			apikey = viper.GetString("cherwell.apikey")
			user   = viper.GetString("cherwell.user")
			pass   = viper.GetString("cherwell.pass")
			ssl    = viper.GetBool("cherwell.ssl")

			a *auth.Auth
		)

		if url == "" {
			fmt.Fprintln(os.Stderr, "no server url specified")
			return
		}

		if ssl {
			a = auth.NewAuthSSL(url, apikey, &user, &pass)
		} else {
			a = auth.NewAuth(url, apikey, &user, &pass)
		}

		token, err := a.BearerTokenString()
		if err != nil {
			fmt.Fprintln(os.Stderr, "could not get token:", err)
			return
		}
		fmt.Println(token)
	},
}

func init() {
	RootCmd.AddCommand(keyCmd)
}
