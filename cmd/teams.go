// Copyright © 2017 Mark Feller <mjfeller1992@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// teamsCmd represents the teams command
var teamsCmd = &cobra.Command{
	Use:   "teams",
	Short: "list all teams in Cherwell",
	Run: func(cmd *cobra.Command, args []string) {
		s := openSession()

		teams, err := s.Teams()
		if err != nil {
			fmt.Fprintln(os.Stderr, "could not get object names:", err)
		}

		if viper.GetBool("teams.json") {
			json.NewEncoder(os.Stdout).Encode(teams)
		} else {
			for _, t := range teams {
				fmt.Println(t.TeamName)
			}
		}
	},
}

func init() {
	RootCmd.AddCommand(teamsCmd)

	teamsCmd.Flags().Bool("json", false, "format output as json")
	viper.BindPFlag("teams.json", teamsCmd.Flags().Lookup("json"))
}
