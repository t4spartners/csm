{{ define "object" }}package fabric

import (
    "strings"

    "github.com/go-openapi/runtime"
    httptransport "github.com/go-openapi/runtime/client"
    "github.com/go-openapi/runtime/middleware"
    "gitlab.com/t4spartners/spindle/server/models"
    "gitlab.com/t4spartners/spindle/server/restapi/operations/{{ snakize .Name }}"
    csm "gitlab.com/t4spartners/csm-go-lib"
)

type {{ .Name }}Handler struct {
    object *csm.BusinessObjectService
}

func New{{ .Name }}Handler(principle interface{}) *{{ .Name }}Handler {
    service := csm.NewBusinessObjectService().WithConn(Connection(principle))
    service.WithAuth(func() runtime.ClientAuthInfoWriter { return httptransport.BearerToken(principle.(string)) }).WithObjectType("{{ .Name }}")

    return &{{ .Name }}Handler{
        object: service,
    }
}

func (h *{{ .Name }}Handler) HandleCount{{ .Name }}(p {{ snakize .Name }}.Count{{ .Name }}Params) middleware.Responder {
    i, err := h.object.Count()
    if err != nil {
        return {{ snakize .Name}}.NewCount{{ .Name }}BadRequest().WithPayload(&models.Badrequest{Error: err.Error()})
    }
    return {{ snakize .Name }}.NewCount{{ .Name }}OK().WithPayload(&models.Count{Count: i})
}

func (h *{{ .Name }}Handler) HandleCreate{{ .Name }}(p {{ snakize .Name }}.Create{{ .Name }}Params) middleware.Responder {
    // Check ID doesn't exists. Remove ID if exists?
    err := h.object.Save(p.Data)
    if err != nil {
        return {{ snakize .Name}}.NewCreate{{ .Name }}BadRequest().WithPayload(&models.Badrequest{Error: err.Error()})
    }
    return {{ snakize .Name }}.NewCreate{{ .Name }}OK().WithPayload(p.Data)
}

func (h *{{ .Name }}Handler) HandleDelete{{ .Name }}(p {{ snakize .Name }}.Delete{{ .Name }}Params) middleware.Responder {
    err := h.object.Delete(p.ID)
    if err != nil {
        return {{ snakize .Name}}.NewDelete{{ .Name }}BadRequest().WithPayload(&models.Badrequest{Error: err.Error()})
    }
    return {{ snakize .Name }}.NewDelete{{ .Name }}OK()
}

func (h *{{ .Name }}Handler) HandleExists{{ .Name }}(p {{ snakize .Name }}.Exists{{ .Name }}Params) middleware.Responder {
    results := []*models.{{ .Name }}{}
    err := h.object.Find(&results)
    if err != nil {
        return {{ snakize .Name}}.NewExists{{ .Name }}BadRequest().WithPayload(&models.Badrequest{Error: err.Error()})
    }
    return {{ snakize .Name }}.NewExists{{ .Name }}OK().WithPayload(&models.Exists{Exists: len(results) != 0})
}

func (h *{{ .Name }}Handler) HandleFind{{ .Name }}(p {{ snakize .Name }}.Find{{ .Name }}Params) middleware.Responder {
    results := []*models.{{ .Name }}{}
    var err error

    if p.Where != nil {
        h.object.WhereJSON(*p.Where)
    }

    for _, s := range p.Order {
        o := strings.Split(s, " ")
        if len(o) == 0 {
            continue
        } else if len(o) == 1 {
            h.object.Order(o[0], csm.Asc)
        } else {
            h.object.Order(o[0], csm.StringToOrder(o[1]))
        }
    }

    if p.Limit != nil {
        h.object.Limit(int(*p.Limit))
    }

    if p.Pagenum != nil {
        h.object.SetPageNumber(int(*p.Pagenum))
    }

    if p.Pagesize != nil {
        h.object.SetPageSize(int(*p.Pagesize))
    }

    err = h.object.Find(&results)

    if err != nil {
        return {{ snakize .Name}}.NewFind{{ .Name }}BadRequest().WithPayload(&models.Badrequest{Error: err.Error()})
    }
    return {{ snakize .Name }}.NewFind{{ .Name }}OK().WithPayload(results)
}

func (h *{{ .Name }}Handler) HandleFindByID{{ .Name }}(p {{ snakize .Name }}.FindByID{{ .Name }}Params) middleware.Responder {
    results := []*models.{{ .Name }}{}
    err := h.object.Where("RecID", csm.Eq, p.ID).Find(&results)
    if err != nil {
        return {{ snakize .Name}}.NewFindByID{{ .Name }}BadRequest().WithPayload(&models.Badrequest{Error: err.Error()})
    }
    if len(results) == 0 {
       return {{ snakize .Name }}.NewFindByID{{ .Name }}OK().WithPayload(nil)
    }
    return {{ snakize .Name }}.NewFindByID{{ .Name }}OK().WithPayload(results[0])
}

func (h *{{ .Name }}Handler) HandleFindOne{{ .Name }}(p {{ snakize .Name }}.FindOne{{ .Name }}Params) middleware.Responder {
    results := []*models.{{ .Name }}{}
    err := h.object.Limit(1).Find(&results)
    if err != nil {
        return {{ snakize .Name}}.NewFindOne{{ .Name }}BadRequest().WithPayload(&models.Badrequest{Error: err.Error()})
    }
    return {{ snakize .Name }}.NewFindOne{{ .Name }}OK().WithPayload(results[0])
}

func (h *{{ .Name }}Handler) HandleUpsert{{ .Name }}(p {{ snakize .Name }}.Upsert{{ .Name }}Params) middleware.Responder {
    // Check ID exists. Check if record exists?
    err := h.object.Save(p.Data)
    if err != nil {
        return {{ snakize .Name}}.NewUpsert{{ .Name }}BadRequest().WithPayload(&models.Badrequest{Error: err.Error()})
    }
    return {{ snakize .Name }}.NewUpsert{{ .Name }}OK().WithPayload(p.Data)
}

func (h *{{ .Name }}Handler) HandleUpdateByID{{ .Name }}(p {{ snakize .Name }}.UpdateByID{{ .Name }}Params) middleware.Responder {
    p.Data.RecID = p.ID
    err := h.object.Save(p.Data)
    if err != nil {
        return {{ snakize .Name}}.NewFind{{ .Name }}BadRequest().WithPayload(&models.Badrequest{Error: err.Error()})
    }
    return {{ snakize .Name }}.NewUpdateByID{{ .Name }}OK().WithPayload(p.Data)
}
{{ end }}
