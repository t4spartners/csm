package server

import (
	"errors"
	"fmt"
	"log"
	"os"
	"text/template"

	csm "gitlab.com/t4spartners/csm-go-lib"
	"gitlab.com/t4spartners/csm-go-lib/auth"
	"gitlab.com/t4spartners/csm-go-lib/client/business_object"
	"gitlab.com/t4spartners/csm-go-lib/models"
	"gitlab.com/t4spartners/csm/generator"
)

type Util struct {
	config  *generator.Config
	session *csm.Session
	auth    *auth.Auth
}

func NewUtil(c *generator.Config) *Util {
	return &Util{
		config: c,
		con:    csm.NewConnection(c.Server.Url),
		auth:   auth.NewAuth(c.Server.Url, c.Server.Apikey, &c.Server.User, &c.Server.Pass),
	}
}

func (u *Util) GetConfigSchemas() ([]*models.SchemaResponse, error) {
	model := make([]*models.SchemaResponse, 0)
	for _, o := range u.config.Include {
		schema, _ := u.getObjectSchema(&o)
		model = append(model, schema)
	}
	return model, nil
}

func (u *Util) getObjectSchema(o *generator.Object) (*models.SchemaResponse, error) {
	params := business_object.NewBusinessObjectGetBusinessObjectSummaryByNameV1Params()
	params.SetBusobname(o.Name)

	w, err := u.auth.BearerToken()
	if err != nil {
		return nil, err
	}
	sum, err := u.con.BusObSummary(params, w)
	check(err)
	if len(sum.Payload) == 0 {
		log.Fatal("Could not find object summary: ", o.Name)
	}

	param := business_object.NewBusinessObjectGetBusinessObjectSchemaV1Params()
	param.SetBusobID(sum.Payload[0].BusObID)
	resp, err := u.con.BusObSchema(param, w)
	check(err)

	return resp.Payload, nil
}

func ExecTemplate(model []*models.SchemaResponse, tmpl *template.Template) error {
	return tmpl.ExecuteTemplate(os.Stdout, "base", model)
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func checkOrCreateDir(path string) error {
	ok, err := exists(path)
	if err != nil {
		return err
	}

	if ok {
		info, err := os.Stat(path)
		if err != nil {
			return err
		}
		if !info.IsDir() {
			return errors.New("target is not a directory: " + path)
		}
	} else {
		fmt.Println("Creating target directory:", path)
		if err := os.MkdirAll(path, 0711); err != nil {
			return err
		}
	}
	return nil
}

// exists returns whether the given file or directory exists or not
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
