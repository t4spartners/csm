package server

import (
	"fmt"
	"os"
	"strings"
	"text/template"

	"github.com/go-openapi/swag"
	"gitlab.com/t4spartners/csm/generator"
)

type FabricBuilder struct {
	Config *generator.Config
	tmpl   *template.Template
}

func (f *FabricBuilder) initAssets() {
	var FuncMap template.FuncMap = map[string]interface{}{
		"pascalize": generator.Pascalize,
		"snakize":   swag.ToFileName,
	}

	data := []string{
		string(MustAsset("templates/object.gotmpl")),
		string(MustAsset("templates/connection.gotmpl")),
	}
	f.tmpl = template.Must(template.New("tmpl").Funcs(FuncMap).Parse(strings.Join(data, "")))
}

func (f *FabricBuilder) ExecTemplate(path string) error {
	outdir := path + "/" + "fabric"
	f.initAssets()
	checkOrCreateDir(outdir)

	for _, o := range f.Config.Include {
		outfile := outdir + "/" + swag.ToFileName(o.Name) + ".go"

		file, err := os.OpenFile(outfile, os.O_RDWR|os.O_CREATE, 0644)
		if err != nil {
			return err
		}
		defer file.Close()

		fmt.Println("Writing to", outfile)
		f.tmpl.ExecuteTemplate(file, "object", o)
	}

	outfile := outdir + "/connection.go"

	file, err := os.OpenFile(outfile, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	fmt.Println("Writing to", outfile)
	f.tmpl.ExecuteTemplate(file, "connection", f.Config.Server)

	return nil
}
